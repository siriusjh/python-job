days_of_week = ["Mon", "Tue", "Wed", "Thur", "Fri"]

 # method : 데이터와 연관된 function

days_of_week.reverse()

days_tuples = ("Mon", "Tue", "Wed") # 불변성

player = {
  'name': 'nico',
  'age': 12,
  'alive': True,
  'fav_food': ["🍟", "🥩"],
  "friend": {
    "name": "lynn",
    "fav_food": ["🌮","🥫"]
  }
  
}

print(player.get('age'))
print(player['fav_food'])
print(player)
player.pop('age')
print(player)
player['xp'] = 1500
player['fav_food'].append("🍱")
print(player)
print(player['friend']['fav_food'])
