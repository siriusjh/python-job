def say_hello(user_name, user_age):
  print("hello", user_name, "how r you?")
  print("you are", user_age, "years old")

say_hello("nico", 12)
say_hello("lewis", 35)

def tax_calculator(money=100):
  return money * 0.35

tax_calculator(150)
tax_calculator()

def pay_tax(tax):
  print("thank you for paying", tax)

to_pay = tax_calculator(200)

pay_tax(to_pay)

def plus(a, b):
  print(a+b)

plus(5,8)

my_name = "nico"
my_age = 12
print(f"hello i'm {my_name}, i have {my_age} years")
