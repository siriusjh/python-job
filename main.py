from requests import get

websites = (
  "google.com",
  "airbnb.com",
  "https://twitter.com",
  "facebook.com"
)

results = {}


for website in websites:
  # print(website)
  if not website.startswith("https://"):
    website = f"https://{website}"
  response = get(website)
  print(response.status_code)
  if response.status_code == 200:
    # print(f"{website} is OK") 
    results[website] = 'OK'
  else:
    # print(f"{website} is not OK") 
    results[website] = 'FAILD'

print(results)


